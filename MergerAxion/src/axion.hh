#ifndef __MergerAxion_h
#define __MergerAxion_h

#include <math.h>

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Functions.h"
#include "util_Table.h"


void MergerAxion_setup(CCTK_ARGUMENTS);


#endif

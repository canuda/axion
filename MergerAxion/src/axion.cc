#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cmath>
#include <fstream>

#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#include "loopcontrol.h"

#include "axion.hh"

using namespace std;


static inline CCTK_REAL SQUARE(CCTK_REAL x) {return x*x;}


void MergerAxion_setup(CCTK_ARGUMENTS)
{
	DECLARE_CCTK_ARGUMENTS
	DECLARE_CCTK_PARAMETERS
	

	int const ni = cctkGH->cctk_lsh[0];
	int const nj = cctkGH->cctk_lsh[1];
  	int const nk = cctkGH->cctk_lsh[2];

	/*CCTK_REAL const R22 = cos(rotation_angle);
  	CCTK_REAL const R23 = -sin(rotation_angle);
 	CCTK_REAL const R32 = sin(rotation_angle);
  	CCTK_REAL const R33 = cos(rotation_angle);*/
    

	
	CCTK_VInfo(CCTK_THORNSTRING,"Setting up MergerAxion");
    
    int num1=5;
    int num2=num_points;
    /* int num3=35;*/
    
    int ix1;
    int ix2;
    double a;
    double data[num1][num2];
    double data2[num1][num2];
    
    double rh1[num2];
    
    double Phi_out1[num2];
    double Psi_out1[num2];
    double drPhi_out1[num2];
    double alp_out1[num2];
    
    double Phi_out2[num2];
    double Psi_out2[num2];
    double drPhi_out2[num2];
    double alp_out2[num2];
    
    double u;
    double t;
    double gamma_boost;
   /* double beta_boost;*/

	//Set boost velocity beta;
	/*beta_boost=0.1;*/
	gamma_boost = 1.0/sqrt(1.0 -pow(beta_boost,2.0));

  
    /*char *inname = filename;*/
    ifstream infile1(filename1);
    
   /* cout << "Opened " << inname << " for reading." << endl;*/
   /* for(int k = 0; k <num3; k++){*/
        for(int j = 0; j <num2; j++){
            for(int i = 0; i <num1; i++){
                infile1 >> a;
                data[i][j]=a;
                /*   printf("%i\n",i); printf("%i\n",j);printf("%i\n",k); cout<<"  position and the data is "; printf("%e\n",data[i][j][k]);
                 cout << "   ";*/
            }
        }
    /*}
    for(int k = 0; k <num3; k++){*/
        for(int j = 0; j <num2; j++){
            rh1[j] = data[0][j];
            Phi_out1[j] = data[1][j];
            Psi_out1[j] = data[2][j];
            alp_out1[j] = data[3][j];
            drPhi_out1[j] = data[4][j];
            
        }
    /*}*/
    
    
    /*char *inname = filename;*/
    ifstream infile2(filename2);
    
    /* cout << "Opened " << inname << " for reading." << endl;*/
    /* for(int k = 0; k <num3; k++){*/
    for(int j = 0; j <num2; j++){
        for(int i = 0; i <num1; i++){
            infile2 >> a;
            data2[i][j]=a;
            /*   printf("%i\n",i); printf("%i\n",j);printf("%i\n",k); cout<<"  position and the data is "; printf("%e\n",data[i][j][k]);
             cout << "   ";*/
        }
    }
    /*}
     for(int k = 0; k <num3; k++){*/
    for(int j = 0; j <num2; j++){
        rh1[j] = data2[0][j];
        Phi_out2[j] = data2[1][j];
        Psi_out2[j] = data2[2][j];
        alp_out2[j] = data2[3][j];
        drPhi_out2[j] = data2[4][j];
        
    }
    /*}*/
    
    
   CCTK_VInfo(CCTK_THORNSTRING,"Finishing reading file");
    
    

			LC_LOOP3(calc_J_components, i, j, k, 0, 0, 0, ni, nj, nk, ni, nj, nk)
		{
            
			CCTK_INT i3D = CCTK_GFINDEX3D(cctkGH, i, j, k);
			CCTK_REAL xx, yy, zz;
			
			CCTK_REAL R, costheta, sintheta;//the radial coordinate introduced by Liu et al 2010
            CCTK_REAL cosphi, sinphi;
			CCTK_REAL rho, rho_sq;
            CCTK_REAL R1, R_sq1;
            CCTK_REAL R2, R_sq2;
            CCTK_REAL rho1, rho2, rho_sq1, rho_sq2;
            CCTK_REAL R1_g, R_sq1_g;
            CCTK_REAL R2_g, R_sq2_g;
            CCTK_REAL rho1_g, rho2_g, rho_sq1_g, rho_sq2_g;
            CCTK_REAL theta1, theta2, sintheta1, sintheta2, sinth_sq1, sinth_sq2;
			CCTK_REAL r_plus, r_minus, r_BL, Sigma, A, Delta;
			CCTK_REAL gii, gjj, gkk;//spatial metric, i=r, j=theta, k=phi
            
            CCTK_REAL gxx0, gyy0, gzz0;//spatial metric, i=r, j=theta, k=phi
            CCTK_REAL gxx1, gyy1, gzz1;//spatial metric, i=r, j=theta, k=phi
            CCTK_REAL gxx2, gyy2, gzz2;//spatial metric, i=r, j=theta, k=phi
            
			CCTK_REAL Kik, Kjk;//extrinsic curvature
			CCTK_REAL betak;//shift
			CCTK_REAL a_sq, costh_sq, sinth_sq;
			CCTK_REAL r_BL_sq, R_sq;
			CCTK_REAL detg;
                        CCTK_REAL det_metric, sdetg;
                        CCTK_REAL guxx, guxy, guxz, guyy, guyz, guzz;
            
            CCTK_REAL theta, phi;
            
            CCTK_REAL t1_boost, t2_boost;
            
            CCTK_REAL Phi_aux1, Pi_aux1, Psi_aux1, Psi4_1;
            CCTK_REAL Phi_aux2, Pi_aux2, Psi_aux2, Psi4_2;
            CCTK_REAL Phi_new1, Phi_new2, Pi_new1, Pi_new2;
            CCTK_REAL alp_aux1, alp_aux2, alp_aux, betay_aux1, betay_aux2;
            CCTK_REAL alp_new1, alp_new2;
            
            CCTK_REAL drPhi_aux1, drPhi_aux2;
            
            CCTK_REAL Efieldli, Efieldlj, Efieldlk;
            
            CCTK_REAL Efieldlx, Efieldly, Efieldlz;
            
            
			CCTK_REAL jac11, jac12, jac13, jac21, jac22, jac23, jac31, jac32;//elements
			//of the coordinate transformation matrix from spherical polar to Cartesian coordinates
			//jac33 is zero for this transformation!
            
			CCTK_REAL g11, g12, g13, g22, g23, g33;////////////////////////////////////////////////
			CCTK_REAL k11, k12, k13, k22, k23, k33;//variables for the rotation of the spacetime!//
			CCTK_REAL beta1, beta2, beta3;/////////////////////////////////////////////////////////
        
			CCTK_REAL gtt;

				xx = x[i3D];
				yy = y[i3D];
				zz = z[i3D];
            
                    /*   r_plus = mass + sqrt(SQUARE(mass) - a_sq);
            r_minus = mass - sqrt(SQUARE(mass) - a_sq);
            
            r_BL = R * SQUARE(1.0 + r_plus/(4.0 * R));//the choice in Liu et al 2010, eq 11
            r_BL_sq = SQUARE(r_BL);
            
            Sigma = r_BL_sq + a_sq * costh_sq;
            Delta = r_BL_sq - 2.0 * mass * r_BL + a_sq;
            A = SQUARE(r_BL_sq + a_sq) - Delta * a_sq * sinth_sq; */
            
            rho_sq = SQUARE(xx)+SQUARE(yy);
            
            rho = sqrt(rho_sq);
            
            gamma_boost = 1.0/sqrt(1.0 -pow(beta_boost,2.0));
            
            
            
            if(rho<epsilon)
                
            {
                xx = epsilon;
                rho_sq = SQUARE(xx)+SQUARE(yy);
                rho = sqrt(rho_sq);
            }
            
            
            R_sq = rho_sq+SQUARE(zz);
            
            R = sqrt(R_sq);
            
            if(R == 0.0)
                
            {
                CCTK_VInfo(CCTK_THORNSTRING,"R is still zero, something went wrong!");
            }
            
            
            
            costheta = zz / R;
            sintheta = rho / R;
            
            theta = abs(atan(rho/zz));
            
           /* if((z[i3D]<0.0)){
                theta = 3.14159265358979323846 - acos(costheta);
            }*/
            
            phi = atan(yy/xx);
            
            if((xx<0.0)){
                phi = atan(yy/xx) + 3.14159265358979323846;
            }
            
            cosphi = cos(mode*phi);
            sinphi = sin(mode*phi);
            
            /*  a_sq = SQUARE(a);*/
            costh_sq = SQUARE(costheta);
            sinth_sq = SQUARE(sintheta);
            
            
            /* Merger */
            rho1 = sqrt(SQUARE(xx - x01 + epsilon) + SQUARE(yy));
            rho2 = sqrt(SQUARE(xx + x02 + epsilon) + SQUARE(yy));
            
            rho_sq1 = SQUARE(xx - x01 + epsilon) + SQUARE(yy);
            rho_sq2 = SQUARE(xx + x02 + epsilon) + SQUARE(yy);
            
            R_sq1 = SQUARE(xx - x01 + epsilon) + SQUARE(yy) + SQUARE(zz);
            R_sq2 = SQUARE(xx + x02 + epsilon) + SQUARE(yy) + SQUARE(zz);
            
            R1 = sqrt(SQUARE(xx - x01 + epsilon) + SQUARE(yy) + SQUARE(zz));
            R2 = sqrt(SQUARE(xx + x02 + epsilon) + SQUARE(yy) + SQUARE(zz));
            
            R_sq1_g = SQUARE(xx - x01 + epsilon) + SQUARE(gamma_boost*(yy - y01)) + SQUARE(zz);
            R_sq2_g = SQUARE(xx + x02 + epsilon) + SQUARE(gamma_boost*(yy + y02)) + SQUARE(zz);
            
            R1_g = sqrt(SQUARE(xx - x01 + epsilon) + SQUARE(gamma_boost*(yy - y01)) + SQUARE(zz));
            R2_g = sqrt(SQUARE(xx + x02 + epsilon) + SQUARE(gamma_boost*(yy + y02)) + SQUARE(zz));
            
            rho1_g = sqrt(SQUARE(xx - x01 + epsilon) + SQUARE(gamma_boost*(yy - y01)));
            rho2_g = sqrt(SQUARE(xx + x02 + epsilon) + SQUARE(gamma_boost*(yy + y02)));
            
            rho_sq1_g = SQUARE(xx - x01 + epsilon) + SQUARE(gamma_boost*(yy - y01));
            rho_sq2_g = SQUARE(xx + x02 + epsilon) + SQUARE(gamma_boost*(yy + y02));
            
            theta1 = abs(atan(rho1_g/zz));
            theta2 = abs(atan(rho2_g/zz));
            
            sintheta1 = rho1_g / R1_g;
            sintheta2 = rho2_g / R2_g;
            
            sinth_sq1 = SQUARE(sintheta1);
            sinth_sq2 = SQUARE(sintheta2);
            
            
            t=0;
            for(int i=0;i<num2-1;i++){
                if((rh1[i]<R1_g) && (rh1[i+1]>R1_g)){
                    t = (R1_g-rh1[i])/(rh1[i+1]-rh1[i]);
                    ix1= i;
                }}
            
            
            Phi_aux1 = (1.0-t)*Phi_out1[ix1] + t*Phi_out1[ix1+1];
            drPhi_aux1 = (1.0-t)*drPhi_out1[ix1] + t*drPhi_out1[ix1+1];
            Psi_aux1 = (1.0-t)*Psi_out1[ix1] + t*Psi_out1[ix1+1];
            alp_aux1 = (1.0-t)*alp_out1[ix1] + t*alp_out1[ix1+1];
            
            
            
            t=0;
            for(int i=0;i<num2-1;i++){
                if((rh1[i]<R2_g) && (rh1[i+1]>R2_g)){
                    t = (R2_g-rh1[i])/(rh1[i+1]-rh1[i]);
                    ix2= i;
                }}
            
            
            Phi_aux2 = (1.0-t)*Phi_out2[ix2] + t*Phi_out2[ix2+1];
            drPhi_aux2 = (1.0-t)*drPhi_out2[ix2] + t*drPhi_out2[ix2+1];
            Psi_aux2 = (1.0-t)*Psi_out2[ix2] + t*Psi_out2[ix2+1];
            alp_aux2 = (1.0-t)*alp_out2[ix2] + t*alp_out2[ix2+1];
        
            
            		//My osservation: I need alp here because I want to calculate the E, A and phi in the spherical coordinate
			//So I will leave it here like it is, and I will boost only later when I have everything in cartesian.
			//Remember that if I want to plot alpha I should consider that alp'=sqrt(gtt') in the boosted system.

            /*if(CCTK_Equals(initial_lapse,"merger-proca"))	{
                alp[i3D] = exp(F0_aux1) + exp(F0_aux2) - 1.0;
                alp_aux = alp[i3D];
                alp_aux1 = exp(F0_aux1);
                alp_aux2 = exp(F0_aux2);
            }*/
            
            if(CCTK_Equals(initial_lapse,"axion-one"))	{
                alp[i3D] = 1.0;
                alp_aux1 = 1.0;
                alp_aux2 = 1.0;
            }
            
            betak = 0.0;
            
            /* Flat */
            
            gxx0 = +1.0;
            gyy0 = +1.0;
            gzz0 = +1.0;
            
            Psi4_1 = pow(Psi_aux1,4);
            Psi4_2 = pow(Psi_aux2,4);
            
            /* PS1 */
            gxx1 = pow(Psi_aux1,4);
            gyy1 = pow(Psi_aux1,4);
            gzz1 = pow(Psi_aux1,4);
            
            /* PS2 */
            gxx2 = pow(Psi_aux2,4);
            gyy2 = pow(Psi_aux2,4);
            gzz2 = pow(Psi_aux2,4);
            
            
            alp[i3D] = alp_aux1/(gamma_boost*sqrt(1.0 - pow(alp_aux1*beta_boost,2)/(Psi4_1)))
                     + alp_aux2/(gamma_boost*sqrt(1.0 - pow(alp_aux2*beta_boost,2)/(Psi4_2)))
                     - 1.0;
            
            betay[i3D] = (pow(alp_aux1,2) - (Psi4_1))/((Psi4_1) - pow(alp_aux1,2)*pow(beta_boost,2))*beta_boost
                       - (pow(alp_aux2,2) - (Psi4_2))/((Psi4_2) - pow(alp_aux2,2)*pow(beta_boost,2))*beta_boost;
            
            gyy[i3D] = (Psi4_1)*pow(gamma_boost,2)*(1.0 - (pow(alp_aux1*beta_boost,2))/(Psi4_1))
                     + (Psi4_2)*pow(gamma_boost,2)*(1.0 - (pow(alp_aux2*beta_boost,2))/(Psi4_2))
                     - 1.0;
            
            betay_aux1 = + (pow(alp_aux1,2) - (Psi4_1))/((Psi4_1) - pow(alp_aux1,2)*pow(beta_boost,2))*beta_boost;
            betay_aux2 = - (pow(alp_aux2,2) - (Psi4_2))/((Psi4_2) - pow(alp_aux2,2)*pow(beta_boost,2))*beta_boost;

            alp_new1 = alp_aux1/(gamma_boost*sqrt(1.0 - pow(alp_aux1*beta_boost,2)/(Psi4_1)));
            alp_new2 = alp_aux2/(gamma_boost*sqrt(1.0 - pow(alp_aux2*beta_boost,2)/(Psi4_2)));
    
            
            /* PS1 */
            
            
            t1_boost = - gamma_boost*beta_boost*yy*omega1;
            t2_boost = + gamma_boost*beta_boost*yy*omega2;
            
            Phi_new1 = Phi_aux1 * cos(t1_boost) + Phi_aux2 * cos(t2_boost);
            Pi_new1  = gamma_boost * omega1 * Phi_aux1 * (+betay_aux1 * beta_boost + 1.0) * sin(t1_boost)/alp_new1 + gamma_boost * gamma_boost * yy * (betay_aux1 + beta_boost) * drPhi_aux1 * cos(t1_boost)/(alp_new1 * R1_g) +
                       gamma_boost * omega2 * Phi_aux2 * (-betay_aux2 * beta_boost + 1.0) * sin(t2_boost)/alp_new2 + gamma_boost * gamma_boost * yy * (betay_aux2 - beta_boost) * drPhi_aux2 * cos(t2_boost)/(alp_new2 * R2_g);
            
            /* PS2 */
            
            
            
            
            Phi_new2 = - Phi_aux1 * sin(t1_boost) - Phi_aux2 * sin(t2_boost);
            Pi_new2  = gamma_boost * omega2 * Phi_aux2 * (-betay_aux2 * beta_boost + 1.0) * cos(t2_boost)/alp_new2 - gamma_boost * gamma_boost * yy * (betay_aux2 - beta_boost) * drPhi_aux2 * sin(t2_boost)/(alp_new2 * R2_g) +
                       gamma_boost * omega1 * Phi_aux1 * (+betay_aux1 * beta_boost + 1.0) * cos(t1_boost)/alp_new1 - gamma_boost * gamma_boost * yy * (betay_aux1 + beta_boost) * drPhi_aux1 * sin(t1_boost)/(alp_new1 * R1_g);
            
 			

            phi1[i3D] = Phi_new1;
            Kphi1[i3D]  = Pi_new1;
            
            phi2[i3D] = Phi_new2;
            Kphi2[i3D]  = Pi_new2;
            
            gxx[i3D] = gxx1 + gxx2 - gxx0;//Jac*K_ij*Tranpose[Jac]
            gxy[i3D] = 0.0;
            gxz[i3D] = 0.0;
            gyz[i3D] = 0.0;
            gzz[i3D] = gzz1 + gzz2 - gzz0;
            
            Zeta[i3D]  = 0;
            
            Ax[i3D]    = 0;
            Ay[i3D]    = 0;
            Az[i3D]    = 0;
            
            Aphi[i3D]  = 0;
            
            Efieldli = 0.0;
            Efieldlj = 0.0;
            Efieldlk = (gzz1 + gzz2 - gzz0) * Amp * R *  exp(-pow((R - r_0),2)/pow(sigma,2));
            
            det_metric = gxx[i3D]*gyy[i3D]*gzz[i3D]-gxx[i3D]*gyz[i3D]*gyz[i3D]-gxy[i3D]*gxy[i3D]*gzz[i3D]+gxy[i3D]*gyz[i3D]*gxz[i3D]+gxz[i3D]*gxy[i3D]*gyz[i3D]-gxz[i3D]*gyy[i3D]*gxz[i3D];
            
            guxx = (-gyz[i3D]*gyz[i3D] + gyy[i3D]*gzz[i3D])/det_metric;
            guxy = ( gxz[i3D]*gyz[i3D] - gxy[i3D]*gzz[i3D])/det_metric;
            guxz = (-gxz[i3D]*gyy[i3D] + gxy[i3D]*gyz[i3D])/det_metric;
            guyy = (-gxz[i3D]*gxz[i3D] + gxx[i3D]*gzz[i3D])/det_metric;
            guyz = ( gxy[i3D]*gxz[i3D] - gxx[i3D]*gyz[i3D])/det_metric;
            guzz = (-gxy[i3D]*gxy[i3D] + gxx[i3D]*gyy[i3D])/det_metric;
            
            
            //coordinate transformation to cactus cartesian grid
            jac11 = xx/R;//////////////////////////////////////////////////////////////
            jac12 = xx*zz/(R_sq*rho);//////////////////////////////////////////////////
            jac13 = -yy/rho_sq;////////////////////////////////////////////////////////
            jac21 = yy/R;//////////////////////////////////////////////////////////////
            jac22 = yy*zz/(R_sq*rho);/////////////Jacobian from Spherical//////////////
            jac23 = xx/rho_sq;/////////////Polar Coordinates to Cartesian Coordinates//
            jac31 = zz/R;//for COVARIANT tensors!!!////////////////////////////////////
            jac32 = -rho/R_sq;/////////////////////////////////////////////////////////
            //jac33 = 0.0;/////////////////////////////////////////////////////////////
            
            Efieldlx = Efieldli*jac11 + Efieldlj*jac12 + Efieldlk*jac13;
            
            Efieldly = Efieldli*jac21 + Efieldlj*jac22 + Efieldlk*jac23;
            
            Efieldlz = Efieldli*jac31 + Efieldlj*jac32;
            
            Ex[i3D] = Efieldlx*guxx + Efieldly*guxy + Efieldlz*guxz;
            Ey[i3D] = Efieldlx*guxy + Efieldly*guyy + Efieldlz*guyz;
            Ez[i3D] = Efieldlx*guxz + Efieldly*guyz + Efieldlz*guzz;
            
            
            
            kxx[i3D] = (-gxx1 + gxx2)*(beta_boost*pow(gamma_boost,2));//Jac*K_ij*Tranpose[Jac]
            kxy[i3D] = 0.0;
            kxz[i3D] = 0.0;
            kyy[i3D] = (-(Psi4_1)*pow(gamma_boost,2)*(1.0 - (pow(alp_aux1*beta_boost,2))/(Psi4_1))
                        +(Psi4_2)*pow(gamma_boost,2)*(1.0 - (pow(alp_aux2*beta_boost,2))/(Psi4_2)))*(beta_boost*pow(gamma_boost,2));
            kyz[i3D] = 0.0;
            kzz[i3D] = (-gzz1 + gzz2)*(beta_boost*pow(gamma_boost,2));
		

        
            
            
			if(CCTK_Equals(initial_shift,"merger-axion"))	{
			/*	betax[i3D] = -yy*betak;
				betay[i3D] = xx*betak;*/
/*                betax[i3D] = betak*gkk*jac13*guxx + betak*gkk*jac23*guxy;
                betay[i3D] = betak*gkk*jac13*guxy + betak*gkk*jac23*guyy;*/
                betax[i3D] = 0.0;
				betaz[i3D] = 0.0;
			}
			
			if(CCTK_Equals(initial_shift,"zero"))	{
				betax[i3D] = 0.0;
				betay[i3D] = 0.0;
				betaz[i3D] = 0.0;
			}
   

		}LC_ENDLOOP3(calc_J_components);

    CCTK_VInfo(CCTK_THORNSTRING,"Done MergerAxion");

		
		
	/*if(fill_past_timelevels){
	CCTK_VInfo(CCTK_THORNSTRING,"Setting past timelevels by copying!");
	
			LC_LOOP3(fill_timelevels, i, j, k, 0, 0, 0, ni, nj, nk, ni, nj, nk)
			{
				CCTK_INT i3D = CCTK_GFINDEX3D(cctkGH, i, j, k);
				
				gxx_p[i3D]   = gxx[i3D];
				gxx_p_p[i3D] = gxx[i3D];
				
				gxy_p[i3D]   = gxy[i3D];
				gxy_p_p[i3D] = gxy[i3D];
				
				gxz_p[i3D]   = gxz[i3D];
				gxz_p_p[i3D] = gxz[i3D];
				
				gyy_p[i3D]   = gyy[i3D];
				gyy_p_p[i3D] = gyy[i3D];
				
				gyz_p[i3D]   = gyz[i3D];
				gyz_p_p[i3D] = gyz[i3D];
				
				gzz_p[i3D]   = gzz[i3D];
				gzz_p_p[i3D] = gzz[i3D];
				
				
				/*kxx_p[i3D]   = kxx[i3D];
				kxx_p_p[i3D] = kxx[i3D];
				
				kxy_p[i3D]   = kxy[i3D];
				kxy_p_p[i3D] = kxy[i3D];
				
				kxz_p[i3D]   = kxz[i3D];
				kxz_p_p[i3D] = kxz[i3D];
				
				kyy_p[i3D]   = kyy[i3D];
				kyy_p_p[i3D] = kyy[i3D];
				
				kyz_p[i3D]   = kyz[i3D];
				kyz_p_p[i3D] = kyz[i3D];
				
				kzz_p[i3D]   = kzz[i3D];
				kzz_p_p[i3D] = kzz[i3D];*//*
				
				
				betax_p[i3D]   = betax[i3D];
				betax_p_p[i3D] = betax[i3D];
				
				betay_p[i3D]   = betay[i3D];
				betay_p_p[i3D] = betay[i3D];
				
				betaz_p[i3D]   = betaz[i3D];
				betaz_p_p[i3D] = betaz[i3D];
				
				alp_p[i3D]   = alp[i3D]; 
				alp_p_p[i3D] = alp[i3D];
                
                Phis1_p[i3D] = Phis1[i3D];
                Phis1_p_p[i3D] = Phis1[i3D];
                
                Phis2_p[i3D] = Phis2[i3D];
                Phis2_p_p[i3D] = Phis2[i3D];
                
                av1lx_p[i3D] = av1lx[i3D];
                av1ly_p[i3D] = av1ly[i3D];
                av1lz_p[i3D] = av1lz[i3D];
            
                av2lx_p[i3D] = av2lx[i3D];
                av2ly_p[i3D] = av2ly[i3D];
                av2lz_p[i3D] = av2lz[i3D];
                
                av1lx_p_p[i3D] = av1lx[i3D];
                av1ly_p_p[i3D] = av1ly[i3D];
                av1lz_p_p[i3D] = av1lz[i3D];
                
                av2lx_p_p[i3D] = av2lx[i3D];
                av2ly_p_p[i3D] = av2ly[i3D];
                av2lz_p_p[i3D] = av2lz[i3D];
                
                
				
			}LC_ENDLOOP3(fill_timelevels);
		

	}*/
	return;
}
